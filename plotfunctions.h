#ifndef PLOTFUNCTIONS_H
#define PLOTFUNCTIONS_H
#include <QVBoxLayout>
#include <QGridLayout>
#include <QVector>
#include "usings.h"
#include "com.h"
#include <tfrzam.h>
#include <qcustomplot.h>

void addPlotGlob(QVBoxLayout *layout, QVector<double> & data, QVector<double> & time, QString plotname);
void addPlotTFRGlob(QGridLayout *layout, ComplexMatrix & data, QVector<double> & time, QString plotname, int i, double lofreq, double hifreq);


#endif // PLOTFUNCTIONS_H
