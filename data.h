#ifndef DATA_H
#define DATA_H
#include <memory>
#include <QDebug>
#include <QMessageBox>
#include <QMainWindow>
#include <qcustomplot.h>
#include <QEventLoop>
#include <QSpacerItem>
#include <QScrollArea>
#include <QLayout>
#include <QSizePolicy>
#include <QVector>
#include "com.h"

class Data
{
public:
    QMainWindow *ptr;
    QTabWidget *tabBar;
    Data(QTabWidget *tb);
    virtual QString type() = 0;
    virtual void display() = 0;
    virtual int getCount() = 0;
    virtual QString tabname() = 0;
    virtual ~Data() {}
    virtual void* getData() = 0;
    virtual QVector<QString> getNames() = 0;
    virtual QVector<double> getTime() = 0;
    virtual double getFreq() = 0;
};

void setDefaultColor(QWidget *ptr);

#endif // DATA_H
