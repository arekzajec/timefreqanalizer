#include "plotfunctions.h"


void addPlotGlob(QVBoxLayout *layout, QVector<double> & data, QVector<double> & time, QString plotname) {
    QCustomPlot *plot = new QCustomPlot;
    plot->addGraph();
    layout->addWidget(plot);
    plot->graph(0)->setData(time,data);
    plot->xAxis->setRange(time.first(),time.last());
    plot->xAxis->setLabel(Com::TimeSec());
    double miny, maxy;
    miny = *std::min_element(data.begin(),data.end());
    maxy = *std::max_element(data.begin(),data.end());
    plot->yAxis->setRange(miny,maxy);
    plot->yAxis->setLabel(plotname);
    plot->setMinimumHeight(minimumPlotHeight);
    plot->setMinimumWidth(minimumPlotWidth);
    plot->replot();
}

void addPlotTFRGlob(QGridLayout *layout, ComplexMatrix & data, QVector<double> & time, QString plotname, int i, double lofreq, double hifreq) {
    QCustomPlot *plot = new QCustomPlot;
    layout->addWidget(plot,i/2,i%2);
    plot->xAxis2->setLabel(plotname);//nie wiem czemu nie działa
    plot->xAxis->setRange(time.first(),time.last());
    plot->xAxis->setLabel(Com::TimeSec());
    double miny, maxy;
    miny = lofreq;
    maxy = hifreq;
    plot->yAxis->setRange(miny,maxy);
    plot->yAxis->setLabel(Com::FreqHz());
    plot->setMinimumHeight(minimumPlotTFRHeight);
    plot->setMinimumWidth(minimumPlotTFRWidth);

    plot->axisRect()->setupFullAxesBox(true);
    QCPColorMap *colorMap = new QCPColorMap(plot->xAxis,plot->yAxis);
    colorMap->data()->setSize(data.size(),data[0].size());
    colorMap->data()->setRange(QCPRange(time.first(),time.last()),QCPRange(miny,maxy));
    for (int xI=0;xI<data.size();xI++) {
        for (int yI=0;yI<data[0].size();yI++) {
            colorMap->data()->setCell(xI,yI,data[xI][yI].real());
        }
    }
    QCPColorScale *colorScale = new QCPColorScale(plot);
    plot->plotLayout()->addElement(0,1,colorScale);
    colorScale->setType(QCPAxis::atRight);
    colorMap->setColorScale(colorScale);
    colorMap->setGradient(QCPColorGradient::gpPolar);
    colorMap->rescaleDataRange();
    QCPMarginGroup *marginGroup = new QCPMarginGroup(plot);
    plot->axisRect()->setMarginGroup(QCP::msBottom|QCP::msTop, marginGroup);
    colorScale->setMarginGroup(QCP::msBottom|QCP::msTop, marginGroup);
    plot->rescaleAxes();

    plot->replot();
}
