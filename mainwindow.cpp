#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QDebug>
#include <QMenuBar>

/** TODO:
 *  - solve bug with changing sets and setings signals in dialogs (?? i don't see this bug anymore, check
 *  changing in resampling dialog)
 *  - skalowanie w tabie 'start' można zrobić lepiej (mainwindow.cpp)
 */

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    this->resize(800,640);
    this->setWindowTitle(Com::WindowTitle());
    central = new QWidget;
    mainlayout = new QVBoxLayout;

    iconslayout = new QHBoxLayout;
    tabBar = new QTabWidget;
    tabBar->setTabsClosable(true);

    central->setLayout(mainlayout);
    mainlayout->addLayout(iconslayout);

    iconslayout->setAlignment(Qt::AlignLeft);
    mainlayout->addWidget(tabBar);

    modules.push_back(new ModuleFile(this,&data,tabBar,iconslayout));
    modules.push_back(new ModuleResample(this,&data,tabBar,iconslayout));
    modules.push_back(new ModuleTFR(this,&data,tabBar,iconslayout));

    modules.push_back(new TestModule(this,&data,tabBar,iconslayout));

    createActions();
    createMenus();
    createIcons();


    QWidget *startTab = new QWidget();
    QWidget *startbackground = new QWidget;

    tabBar->addTab(startTab,Com::Start());
    setCentralWidget(central);

    QPixmap title(":/img/Title.png");
    QVBoxLayout *layout = new QVBoxLayout;
    QVBoxLayout *lastlayout = new QVBoxLayout;
    QLabel *labelTitle = new QLabel;
    labelTitle->setPixmap(title);
    labelTitle->setAlignment(Qt::AlignCenter);
    lastlayout->addWidget(labelTitle);
    startTab->setLayout(layout);
    layout->addWidget(startbackground);
    layout->setMargin(0);
    startbackground->setLayout(lastlayout);

    setDefaultColor(startbackground);

    connect(tabBar,SIGNAL(tabCloseRequested(int)),this,SLOT(closeTab(int)));

    this->showMaximized();

}

void MainWindow::createActions() {
    for (auto & it : modules) {
        it->addActions(actions_ptrs);
    }
}

void MainWindow::createMenus() {
    for (auto & it : modules) {
        it->addMenus(menus_ptrs,actions_ptrs);
    }
}

void MainWindow::createIcons() {
    for (auto & it : modules) {
        it->addIcons(icons_ptrs);
    }
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::closeTab(int ind)
{
    if (ind == 0) {
        return;
    }
    QWidget* tabItem = tabBar->widget(ind);

    tabBar->removeTab(ind);

    delete(tabItem);
    tabItem = nullptr;

}
