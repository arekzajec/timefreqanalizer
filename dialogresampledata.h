#ifndef DIALOGRESAMPLEDATA_H
#define DIALOGRESAMPLEDATA_H

#include <QDialog>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QPushButton>
#include <QLineEdit>
#include <QComboBox>
#include <QString>
#include <QSpacerItem>
#include <QCheckBox>
#include <QLabel>
#include <memory>
#include "usings.h"
#include "com.h"
#include "dataraw.h"
#include <resampling.h>

class DialogResampleMethod : public QDialog
{
public:
    DialogResampleMethod(std::shared_ptr<int> _methodInd, std::shared_ptr<int> _ekgInd, std::shared_ptr<double> _newfreq, QVector<QString> _names);

private slots:
    void onChangeMethod(int ind);
    void onChangeEkg(int ind);
    void onSetIdentical(int ind);
    void freqChange(const QString & str);
private:
    std::shared_ptr<int> methodInd;
    std::shared_ptr<double> freq;
    std::shared_ptr<int> ekgInd;
    QVector<QString> names;

    QVBoxLayout *mainbox;
    QGridLayout *gridbox;
    QHBoxLayout *okbox;
    QPushButton *ok;
    QLabel *methodtext;
    QLabel *freqtext;
    QLabel *ekgtext;
    QComboBox *methodBox;
    QLineEdit *freqedit;
    QComboBox *ekgBox;
};

class DialogResampleData : public QDialog
{
public:
    DialogResampleData(std::shared_ptr<int> _setInd, std::shared_ptr<DataCollection> _datacollection, std::shared_ptr<QVector<bool> > _isVisible);

private slots:
    void onChangeSet(int ind);
    void onCheck();

private:
    QVBoxLayout *mainbox;
    QVBoxLayout *boxesbox;
    QHBoxLayout *okbox;
    QPushButton *ok;
    QLabel *setdatatext;
    QLabel *datatext;
    QVector<QCheckBox*> boxes;
    QComboBox *setBox;
    std::shared_ptr<QVector<bool> > isVisible;
    std::shared_ptr<int> setInd;
    std::shared_ptr<DataCollection> datacollection;
};

void clearLayout(QLayout *layout);

#endif // DIALOGRESAMPLEDATA_H
