#include "moduleresample.h"

ModuleResample::ModuleResample(QMainWindow *_ptr, DataCollection *_data, QTabWidget *tb, QHBoxLayout *buttonlay) :
    Module(_ptr, _data, tb, buttonlay)
{

}

void ModuleResample::addActions(Actions &actions) {
    actions["ResampleModule::resampleData"] = std::shared_ptr<QAction>(new QAction(Com::ResampleRawData(),ptr));
    actions["ResampleModule::resampleData"]->setStatusTip(Com::ChooseDataToResampling());
    actions["ResampleModule::resampleData"]->setIcon(QIcon(":/img/icons/resampling.ico"));
    ptr->connect(actions["ResampleModule::resampleData"].get(),&QAction::triggered,this,&ModuleResample::resampleData);
}

void ModuleResample::addMenus(Menus &menus, Actions &actions) {
    menus["ResampleModule"] = std::shared_ptr<QMenu>(ptr->menuBar()->addMenu(Com::Resampling()));
    menus["ResampleModule"].get()->addAction(actions["ResampleModule::resampleData"].get());
}

void ModuleResample::addIcons(Icons &icons) {
    QPushButton *resamplePB = newIcon(QIcon(":/img/icons/resampling.ico"));
    icons["ResampleModule::resampleData"] = std::shared_ptr<QPushButton>(resamplePB);
    buttonslayout->addWidget(resamplePB);
    ptr->connect(resamplePB,&QAbstractButton::clicked,this,&ModuleResample::resampleData);
}

void ModuleResample::resampleData() {
    bool stop = true;
    for (int i=0;i<data->size();i++) {
        if ((*data)[i]->type() == DataRaw::TypeString()) {
            stop = false;
            break;
        }
    }
    if (stop) {
        QMessageBox(QMessageBox::Warning, Com::Warning(), Com::PleaseOpenDataFirst(), QMessageBox::Ok).exec();
        return;
    }


    DataResample *dane = new DataResample(tabBar);
    data->push_back(dane);
    dane->calculate(data);
    dane->display();
}
