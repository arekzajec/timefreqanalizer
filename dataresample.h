#ifndef DATARESAMPLE_H
#define DATARESAMPLE_H

#include "data.h"
#include <QString>
#include <QStringList>
#include <QVector>
#include <limits>
#include <usings.h>
#include <dialogresampledata.h>
#include <plotfunctions.h>

class DataResample : public Data
{
public:
    DataResample(QTabWidget *tb);
    ~DataResample();
private:
    QString _tabname;
    QVector<QString> names;
    QVector<QVector <double> > data;
    std::shared_ptr<double> freq;
    QVector<double> time;
    std::shared_ptr<QVector<bool> > isVisible;
    std::shared_ptr<int> methodInd;
    std::shared_ptr<int> ekgInd;
    std::shared_ptr<int> rawInd;

public:
    static QString TypeString() {return "ResampleData";}
    void calculate(std::shared_ptr<DataCollection> data_collection);
    void display();
    int getCount() { return counter;}
    QString type() {return TypeString();}
    QString tabname() {return _tabname;}
    void* getData();
    QVector<QString> getNames();
    QVector<double> getTime();
    double getFreq();
    void addPlot(QVBoxLayout *layout, int ind);


private:
    static int counter;
};

#endif // DATARESAMPLE_H
