#include "module.h"

Module::Module(QMainWindow *_ptr, DataCollection *_data, QTabWidget *tb, QHBoxLayout *buttonlay) :
    ptr(_ptr),
    data(_data),
    tabBar(tb),
    buttonslayout(buttonlay)
{

}


QPushButton* Module::newIcon(QIcon icon) {
    QPushButton *openPB = new QPushButton(ptr);
    openPB->setIcon(icon);
    openPB->setIconSize(QSize(0.8*iconsize,0.8*iconsize));
    openPB->setFixedHeight(iconsize);
    openPB->setFixedWidth(iconsize);
    //openPB->setStyleSheet("border: none");
    return openPB;
}
