#ifndef DATARAW_H
#define DATARAW_H
#include "data.h"
#include <QString>
#include <QStringList>
#include <QFile>
#include <QVector>
#include <limits>
#include "dialograwdata.h"
#include <plotfunctions.h>

using RealMatrix=QVector<QVector<double> >;

class DataRaw : public Data
{
public:
    DataRaw(QTabWidget *tb);
    ~DataRaw();
private:
    QString _tabname;
    QVector<QString> names;
    QVector<QVector<double> > data;
    std::shared_ptr<QVector<bool> > isVisible;
    std::shared_ptr<int> timeInd;
    std::shared_ptr<double> freq;
    QVector<double> time;
    std::shared_ptr<QVector<QVector<double>>> tmpOutData;
public:
    static QString TypeString() {return "RawData";}
    void readCSVFileData(QFile* file, QString tabname, std::unique_ptr<QMessageBox> _msgbox);
    void display();
    int getCount() { return counter;}
    QString type() {return TypeString();}
    QString tabname() {return _tabname;}
    void* getData();
    QVector<QString> getNames();
    QVector<double> getTime();
    double getFreq();
    void recalculateTime();
    void addPlot(QVBoxLayout *layout, int ind);
private:
    static int counter;
};

#endif // DATARAW_H
