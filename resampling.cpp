#include "resampling.h"

RealMatrix resampleUsingEkg(RealMatrix sig, QVector<double> ekg, double orgfreq, double newfreq) {



}

RealMatrix detrend(RealMatrix sig) {
    RealMatrix y;
    y.reserve(sig.size());
    QVector<double> yi;
    double S, Sx, Sy, Sxx, Sxy, Syy, a, b;
    for (int i=0;i<sig.size();i++) {
        int N=sig[i].size();
        yi.clear();
        S=N;
        Sx=0.0;
        Sy=0.0;
        Sxx=0.0;
        Sxy=0.0;
        Syy=0.0;
        for (int n=0;n<N;n++) {
            Sx+=n;
            Sy+=sig[i][n];
            Sxx+=n*n;
            Syy+=sig[i][n]*sig[i][n];
            Sxy=n*sig[i][n];
        }
        a = (S*Sxy-Sx*Sy)/(S*Sxx-Sx*Sx);
        b = (Sxx*Sy-Sx*Sxy)/(S*Sxx-Sx*Sx);
        for (int n=0;n<N;n++) {
            yi.push_back(sig[i][n]-a*n-b);
        }
        y.push_back(yi);
    }
    return y;
}

RealMatrix pan_tompkin(QVector<double> ekg, double fs) { //qrs_amp, qrs_i, delay
    QVector<double> qrs_amplitude;
    QVector<double> qrs_index;
    QVector<double> qrs_delay;


}


double Epanechnikov(double x) {
    return 0.75*(1-x*x)>0?0.75*(1-x*x):0;
}

QVector<double> NadarayaWatson(QVector<double> x, QVector<double> y, double newfs, double (*ker)(double in), double h) {
    QVector<double> out;
    int N=x.back()*newfs;
    out.reserve(N+1);
    for (int i=0;i<=N;i++) {
        double L=0.0;
        double M=0.0;
        for (int j=0;j<x.size();j++) {
            M+= ker((i/newfs-x[j])/h);
            L+= y[j]*ker((i/newfs-x[j])/h);
        }
        out.push_back(L/M);
    }
    return out;
}


RealMatrix NWresampling(RealMatrix in, double oldfq, double newfq, double (*ker)(double in), double h) {
    RealMatrix out;
    out.reserve(in.size());
    QVector<double> x;
    for (double i=0;i<in[0].size();i++) {
        x.push_back(i/oldfq);
    }
    for (int i=0;i<in.size();i++) {
        out.push_back(NadarayaWatson(x,in[i],newfq,ker,h));
    }
    return out;
}
