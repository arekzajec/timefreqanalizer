#include "datatfr.h"
#include <QDebug>

int DataTFR::counter = 0;

DataTFR::DataTFR(QTabWidget *tb) :
    Data(tb),
    algorithmInd(new int(0)),
    numberOfFrequencyBins(new int(4096)),
    timeSmoothingWindowInd(new int(0)),
    freqSmoothingWindowInd(new int(0)),
    timeSmoothingWindowLength(new int(513)),
    freqSmoothingWindowLength(new int(449)),
    lowfreq(new double(0.02)),
    hifreq(new double(0.07))
{
    counter++;
}

DataTFR::~DataTFR() {
    counter--;
}

void DataTFR::calculate(std::shared_ptr<DataCollection> data_collection) {
    _tabname = Com::TFRdata().append(QString::number(getCount()));
    std::shared_ptr<int> setInd(new int);
    isVisible = std::shared_ptr<QVector<bool>>(new QVector<bool>);

    DialogTFRData *dialogData = new DialogTFRData(setInd,data_collection,isVisible);
    dialogData->exec();

    time = (*data_collection)[*setInd]->getTime();
    freq = (*data_collection)[*setInd]->getFreq();

    DialogTFRParams *dialogParams = new DialogTFRParams(algorithmInd,
                                                        numberOfFrequencyBins,
                                                        timeSmoothingWindowInd,
                                                        freqSmoothingWindowInd,
                                                        timeSmoothingWindowLength,
                                                        freqSmoothingWindowLength,
                                                        lowfreq,
                                                        hifreq);
    dialogParams->exec();
    int size = static_cast<RealMatrix*>((*data_collection)[*setInd]->getData())->size();
    RealMatrix tmpdata;
    for (int i=0;i<size;i++) {
        if ((*isVisible)[i]) {
            names.push_back((*data_collection)[*setInd]->getNames()[i]);
            tmpdata.push_back(static_cast<RealMatrix*>((*data_collection)[*setInd]->getData())->at(i));
        }
    }
    QVector<double> g;
    QVector<double> h;
    switch(*timeSmoothingWindowInd) {
    case 0:{
        g = hanning(*timeSmoothingWindowLength);
    };break;
    default:break;
    }

    switch(*freqSmoothingWindowInd) {
    case 0:{
        h = hanning(*freqSmoothingWindowLength);
    };break;
    default:break;
    }

    switch(*algorithmInd) {
    case 0:{
        ComplexMatrix tmpComplexData = realToComplex(tmpdata);
        ComplexMatrix tmpMx;
        QVector<int> time4tfr;
        for(int i=1;i<=tmpComplexData[0].size();i++) {
            time4tfr.push_back(i);
        }
        for(int i=0;i<tmpComplexData.size();i++) {
            tmpMx.clear();
            tmpMx.push_back(tmpComplexData[i]);
            ComplexMatrix tmpOutMx = tfrzam(tmpMx,time4tfr,*numberOfFrequencyBins,g,h);
            for (int ii=0;ii<tmpOutMx.size();ii++) {
                for (int jj=0;jj<tmpOutMx[0].size();jj++) {
                    if (tmpOutMx[ii][jj].real() < 0) {
                        tmpOutMx[ii][jj] = 0;
                    }
                }
            }
            data.push_back(tmpOutMx);
        }
    };break;
    default:break;
    }

}

void DataTFR::display() {
    QWidget *tfrTab = new QWidget;
    tabBar->addTab(tfrTab,_tabname);
    tabBar->setCurrentWidget(tfrTab);

    setDefaultColor(tfrTab);
    QVBoxLayout *vlayout = new QVBoxLayout;

    QScrollArea *scrollArea = new QScrollArea;

    scrollArea->setVerticalScrollBarPolicy(Qt::ScrollBarAsNeeded);
    scrollArea->setHorizontalScrollBarPolicy(Qt::ScrollBarAsNeeded);
    scrollArea->setWidgetResizable(true);

    QGridLayout *scrollglayout = new QGridLayout;
    for (int i=0;i<data.size();i++) {
        addPlot(scrollglayout,i);
    }

    QWidget *scrollWidget = new QWidget();
    scrollWidget->setLayout(scrollglayout);
    scrollWidget->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);

    scrollArea->setWidget(scrollWidget);
    vlayout->addWidget(scrollArea);
    tfrTab->setLayout(vlayout);
    vlayout->setMargin(0);
    scrollglayout->setMargin(0);
}

void DataTFR::addPlot(QGridLayout *layout, int ind) {
    /*temporary*/

    *lowfreq=0;
    *hifreq=freq/2;

    addPlotTFRGlob(layout,data[ind],time,names[ind],ind,*lowfreq,*hifreq);
}

void *DataTFR::getData() {
    return &data;
}

QVector<QString> DataTFR::getNames() {
    return names;
}

QVector<double> DataTFR::getTime() {
    return time;
}


double DataTFR::getFreq() {
    return freq;
}
