#include "tfrzam.h"
#include <QDebug>

ComplexMatrix tfrzam(ComplexMatrix x, QVector<int> t, int N, QVector<double> g, QVector<double> h) {

/* obsługa złego wejścia do napisania, x.size() = {1,2}, x[0].size() == x[1].size() */
/* okno symetryczne o nieparzystej długości, znormalizowane*/
/* N MUSI być wartością potęgi dwójki! */

    int xrow = x[0].size();
    int xcol = x.size();

    int tcol = t.size();

    int grow = g.size();
    int Lg = (grow-1)/2;
    /*for (int i=1;i<=grow;i++) {
        mIndex(g,i) = mIndex(g,i) / mIndex(g,Lg+1);
    }*/

    int hrow = h.size();
    int Lh = (hrow-1)/2;
    /*for (int i=1;i<=hrow;i++) {
        mIndex(h,i) = mIndex(h,i) / mIndex(h,Lg+1);
    }*/


    ComplexMatrix tfr;
    ComplexVector tmpVecCplx;
    tmpVecCplx.clear();
    qDebug() << "N" << N;
    for (int i=0;i<N;i++) {
        tmpVecCplx.push_back(std::complex<double>(0.0,0.0));
    }
    qDebug() << "tcol" << tcol;
    qDebug() << tfr.size();
    for (int i=0;i<tcol;i++) {
        tfr.push_back(tmpVecCplx);
    }
    qDebug() << tfr.size();
    qDebug() << "!";
    for (int icol=1;icol<=tcol;icol++) {
        int ti = mIndex(t,icol);
        int taumax;
        QVector<int> tmpVecInt1;
        tmpVecInt1.clear();
        QVector<int> tmpVecInt2;
        tmpVecInt2.clear();
        tmpVecInt1.push_back(ti+Lg-1);
        tmpVecInt1.push_back(xrow-ti+Lg);
        tmpVecInt1.push_back(std::round(N/2.0+0.1)-1);//(bledy numeryczne -> logiczne)?
        tmpVecInt1.push_back(Lh);
        //qDebug() << ti+Lg-1 << xrow-ti+Lg << std::round(N/2.0+0.1)-1 << Lh;
        taumax = *std::min_element(tmpVecInt1.begin(),tmpVecInt1.end());
        qDebug() << icol << "taumax: " << taumax;
        mIndex(tfr,icol,1) = mIndex(g,Lg+1) * mIndex(x,1,ti) * std::conj(mIndex(x,xcol,ti));
        for (int tau=1;tau<=taumax;tau++) {
            QVector<int> points;//poczatek linii 67 w matlabie
            points.clear();
            tmpVecInt1.clear();
            tmpVecInt1.push_back(tau);
            tmpVecInt1.push_back(Lg);
            tmpVecInt1.push_back(xrow-ti-tau);
            tmpVecInt2.clear();
            tmpVecInt2.push_back(tau);
            tmpVecInt2.push_back(Lg);
            tmpVecInt2.push_back(ti-tau-1);
            int pointsBegin = -*std::min_element(tmpVecInt1.begin(),tmpVecInt1.end());
            int pointsEnd   = *std::min_element(tmpVecInt2.begin(),tmpVecInt2.end());
            //qDebug() << icol << " " << tau << "pB: " << pointsBegin << "pE: " << pointsEnd;
            for (int i=pointsBegin;i<=pointsEnd;i++) {
                points.push_back(i);
            }//koniec linii 67 w matlabie
            QVector<double> g2;
            g2.clear();
            for (int i=0;i<points.size();i++) {
                g2.push_back(mIndex(g,Lg+1+points[i]));
            }
            std::complex<double> R(0.0,0.0);
            for (int i=0;i<points.size();i++) {
                R+=(std::complex<double>(g2[i],0.0) * mIndex(x,1,ti+tau-points[i]) * std::conj(mIndex(x,xcol,ti-tau-points[i])));
            }
            mIndex(tfr,icol,1+tau) = mIndex(h,Lh+tau+1)*R;
            R=std::complex<double>(0.0,0.0);
            for (int i=0;i<points.size();i++) {
                R+=(std::complex<double>(g2[i],0.0) * mIndex(x,1,ti-tau-points[i]) * std::conj(mIndex(x,xcol,ti+tau-points[i])));
            }
            mIndex(tfr,icol,N+1-tau) = mIndex(h,Lh-tau+1)*R;
        }
    }

    std::valarray<std::complex<double> > tmpValArr(tfr[0].size());
    for (int i=0;i<tfr.size();i++) {
        auto it=std::begin(tmpValArr);
        for (int j=0;j<tfr[i].size();j++) {
            *it++ = tfr[i][j];
        }
        fft(tmpValArr);
        it=std::begin(tmpValArr);
        for (int j=0;j<tfr[i].size();j++) {
            if (xcol==1) {
                tfr[i][j] = (*it++).real();
            } else {
                tfr[i][j] = *it++;
            }
        }
    }


    qDebug() << tfr.size();
    qDebug() << "end";
    return tfr;



}

ComplexMatrix realToComplex(QVector<QVector<double> > in) {
    ComplexMatrix out;
    QVector<std::complex<double> > tmp;
    for (int i=0;i<in.size();i++) {
        tmp.clear();
        for (int j=0;j<in[0].size();j++) {
            tmp.push_back(std::complex<double>(in[i][j],0.0));
        }
        out.push_back(tmp);
    }
    return out;
}

QVector<double> hanning(int length) {
    QVector<double> out;
    for (int n=0;n<length;n++) {
        out.push_back(0.5*(1-std::cos((2*PI*n)/(length-1))));
    }
    return out;
}

template<class typ>
typ & mIndex(QVector<typ> & vec, int i) {
    return vec[i-1];
}

template<class typ>
typ & mIndex(QVector<QVector<typ> > & vec, int i, int j) {
    return vec[i-1][j-1];
}
