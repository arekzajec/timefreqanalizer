#ifndef FFT_H
#define FFT_H
#include <complex>
#include <valarray>
#include "usings.h"

typedef std::complex<double> Complex;
typedef std::valarray<Complex> CArray;
void fft(CArray &x);
void ifft(CArray &x);

#endif // FFT_H
