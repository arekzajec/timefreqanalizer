#ifndef MODULERESAMPLE_H
#define MODULERESAMPLE_H
#include <module.h>
#include <modulefile.h>
#include "dataresample.h"
#include "com.h"
#include "dataraw.h"

class ModuleResample : public Module
{
public:
    ModuleResample();
    QPushButton* resampleicon;

    ModuleResample(QMainWindow *_ptr, DataCollection *_data, QTabWidget *tb, QHBoxLayout *buttonlay);
    void addActions(Actions & actions);
    void addMenus(Menus & menus, Actions & actions);
    void addIcons(Icons & icons);

    void resampleData();
};

#endif // MODULERESAMPLE_H
