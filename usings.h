#ifndef USINGS_H
#define USINGS_H

#include <map>
#include <QString>
#include <QMenu>
#include <memory>
#include <QAction>
#include <data.h>
#include <QTabWidget>
#include <QWidget>
#include <QPixmap>
#include <QLabel>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QPalette>
#include <QApplication>
#include <QRect>
#include <QDesktopWidget>
#include <algorithm>
#include <QPushButton>

const double PI = 3.141592653589793238460;

using Menus = std::map<QString, std::shared_ptr<QMenu> >;
using Actions = std::map<QString, std::shared_ptr<QAction> >;
using DataCollection = QVector<Data*>;
using Icons = std::map<QString, std::shared_ptr<QPushButton> >;
const int iconsize = 50;
const int minimumPlotHeight = 200;
const int minimumPlotWidth = 600;
const int minimumPlotTFRHeight = 500;
const int minimumPlotTFRWidth = 500;

#endif // USINGS_H



