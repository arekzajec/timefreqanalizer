#ifndef MODULETFR_H
#define MODULETFR_H
#include <module.h>
#include <modulefile.h>
#include <dataresample.h>
#include <dataraw.h>
#include <datatfr.h>
#include <com.h>

class ModuleTFR : public Module
{
public:
    ModuleTFR();
    QPushButton* tfricon;

    ModuleTFR(QMainWindow *_ptr, DataCollection *_data, QTabWidget *tb, QHBoxLayout *buttonlay);
    void addActions(Actions & actions);
    void addMenus(Menus & menus, Actions & actions);
    void addIcons(Icons & icons);

    void calculateTFR();
};

#endif // MODULETFR_H
