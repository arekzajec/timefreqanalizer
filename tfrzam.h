#ifndef TFRZAM_H
#define TFRZAM_H

#include <vector>
#include <complex>
#include <algorithm>
#include <cmath>
#include "fft.h"

using ComplexMatrix=QVector<QVector<std::complex<double> > >;
using ComplexVector=QVector<std::complex<double > >;

ComplexMatrix tfrzam(ComplexMatrix x, QVector<int> t, int N, QVector<double> g, QVector<double> h);
ComplexMatrix realToComplex(QVector<QVector<double> > in);
QVector<double> hanning(int length);

template<class typ>
typ & mIndex(QVector<typ> & vec, int i);

template<class typ>
typ & mIndex(QVector<QVector<typ> > & vec, int i, int j);


#endif // TFRZAM_H
