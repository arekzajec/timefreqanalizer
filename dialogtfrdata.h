#ifndef DIALOGTFRDATA_H
#define DIALOGTFRDATA_H

#include <QDialog>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QPushButton>
#include <QLineEdit>
#include <QComboBox>
#include <QString>
#include <QSpacerItem>
#include <QCheckBox>
#include <QLabel>
#include <memory>
#include <usings.h>
#include <com.h>
#include <dataraw.h>
#include <dataresample.h>
#include <dialogresampledata.h>

class DialogTFRParams : public QDialog
{
public:
    DialogTFRParams(std::shared_ptr<int> _algorithmInd,
                    std::shared_ptr<int> _numberOfFrequencyBins,
                    std::shared_ptr<int> _timeSmoothingWindowInd,
                    std::shared_ptr<int> _freqSmoothingWindowInd,
                    std::shared_ptr<int> _timeSmoothingWindowLength,
                    std::shared_ptr<int> _freqSmoothingWindowLength,
                    std::shared_ptr<double> _lowfreq,
                    std::shared_ptr<double> _hifreq);

private slots:
    void onChangeAlg(int ind);
    void onChangeAlgTimeW(int ind);
    void onChangeAlgFreqW(int ind);
    void onChangeFreqBins(const QString & str);
    void onChangeTimeWLength(const QString & str);
    void onChangeFreqWLength(const QString & str);
    void onChangeLowFreq(const QString & str);
    void onChangeHiFreq(const QString & str);

private:
    std::shared_ptr<int> algorithmInd;
    std::shared_ptr<int> numberOfFrequencyBins;
    std::shared_ptr<int> timeSmoothingWindowInd;
    std::shared_ptr<int> freqSmoothingWindowInd;
    std::shared_ptr<int> timeSmoothingWindowLength;
    std::shared_ptr<int> freqSmoothingWindowLength;
    std::shared_ptr<double> lowfreq;
    std::shared_ptr<double> hifreq;

    QVBoxLayout *mainbox;
    QGridLayout *gridbox;
  /*  QGridLayout *gridbox1;
    QGridLayout *gridbox2;
    QGridLayout *gridbox3;
    QGridLayout *gridbox4;*/
    QHBoxLayout *okbox;
   /* QHBoxLayout *hbox1;
    QHBoxLayout *hbox2;*/
    QPushButton *ok;
    QLabel *algorithmText;
    QLabel *freqBinsText;
    QLabel *timeSmoothingWindowText;
    QLabel *freqSmoothingWindowText;
    QLabel *lowfreqText;
    QLabel *hifreqText;
    QLabel *type1;
    QLabel *type2;
    QLabel *length1;
    QLabel *length2;
    QComboBox *algorithmBox;
    QComboBox *timeSmoothingWindowBox;
    QComboBox *freqSmoothingWindowBox;
    QLineEdit *timeSmoothingWindowEdit;
    QLineEdit *freqSmoothingWindowEdit;
    QLineEdit *freqBinsEdit;
    QLineEdit *lowfreqEdit;
    QLineEdit *hifreqEdit;

};

class DialogTFRData : public QDialog
{
public:
    DialogTFRData(std::shared_ptr<int> _setInd, std::shared_ptr<DataCollection> _datacollection, std::shared_ptr<QVector<bool> > _isVisible);

private slots:
    void onChangeSet(int ind);
    void onCheck();

private:
    QVBoxLayout *mainbox;
    QVBoxLayout *boxesbox;
    QHBoxLayout *okbox;
    QPushButton *ok;
    QLabel *setdatatext;
    QLabel *datatext;
    QVector<QCheckBox*> boxes;
    QComboBox *setBox;
    std::shared_ptr<QVector<bool> > isVisible;
    std::shared_ptr<int> setInd;
    std::shared_ptr<DataCollection> datacollection;
};

#endif // DIALOGTFRDATA_H
