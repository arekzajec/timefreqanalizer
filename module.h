#ifndef MODULE_H
#define MODULE_H

#include "usings.h"
#include <QKeySequence>
#include <QMainWindow>
#include <QMenuBar>
#include <QAction>
#include <memory>
#include <QMessageBox>
#include <QPushButton>
#include <QIcon>

#include <QDebug>

/* Please remember that Actions and FunctionSlots must have the same names (first atribute in std::map)
 *
 *
 *
 * */

class Module : public QObject
{
public:
    QMainWindow *ptr;
    std::shared_ptr<DataCollection> data;
    QTabWidget *tabBar;
    QHBoxLayout *buttonslayout;
    Module(QMainWindow *_ptr, DataCollection *_data, QTabWidget *tb, QHBoxLayout *buttonlay);
    virtual void addMenus(Menus & menus, Actions & actions) = 0;
    virtual void addActions(Actions & actions) = 0;
    virtual void addIcons(Icons & icons) = 0;
    QPushButton *newIcon(QIcon icon);
};

#endif // MODULE_H
