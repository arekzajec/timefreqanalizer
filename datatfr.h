#ifndef DATATFR_H
#define DATATFR_H
#include <data.h>
#include <QString>
#include <QStringList>
#include <QVector>
#include <limits>
#include <usings.h>
#include <dialogtfrdata.h>
#include <tfrzam.h>
#include <hilbert.h>
#include <plotfunctions.h>

using ComplexTensor=QVector<ComplexMatrix>;

class DataTFR : public Data
{
public:
    DataTFR(QTabWidget *tb);
    ~DataTFR();
private:
    QString _tabname;
    QVector<QString> names;
    ComplexTensor data;
    double freq;
    QVector<double> time;
    std::shared_ptr<QVector<bool> > isVisible;
    std::shared_ptr<int> algorithmInd;
    std::shared_ptr<int> numberOfFrequencyBins;
    std::shared_ptr<int> timeSmoothingWindowInd;
    std::shared_ptr<int> freqSmoothingWindowInd;
    std::shared_ptr<int> timeSmoothingWindowLength;
    std::shared_ptr<int> freqSmoothingWindowLength;
    std::shared_ptr<double> lowfreq;
    std::shared_ptr<double> hifreq;
    void addPlot(QGridLayout *layout, int ind);

public:
    static QString TypeString() {return "TFRData";}
    void calculate(std::shared_ptr<DataCollection> data_collection);
    void display();
    int getCount() {return counter;}
    QString type() {return TypeString();}
    QString tabname() {return _tabname;}
    void* getData();
    QVector<QString> getNames();
    QVector<double> getTime();
    double getFreq();

private:
    static int counter;
};

#endif // DATATFR_H
