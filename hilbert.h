#ifndef HILBERT_H
#define HILBERT_H

#include <vector>
#include <complex>
#include <cmath>
#include "usings.h"
#include "fft.h"

using ComplexVector=QVector<std::complex<double > >;

ComplexVector hilbert(QVector<double> & v);

#endif // HLIBERT_H
