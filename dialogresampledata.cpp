#include "dialogresampledata.h"

DialogResampleMethod::DialogResampleMethod (std::shared_ptr<int> _methodInd, std::shared_ptr<int> _ekgInd, std::shared_ptr<double> _newfreq, QVector<QString> _names) :
    methodInd(_methodInd),
    ekgInd(_ekgInd),
    freq(_newfreq),
    names(_names)
{
    this->setWindowTitle(Com::ResamplingMethod());

    mainbox = new QVBoxLayout;
    okbox = new QHBoxLayout;
    gridbox = new QGridLayout;
    ok = new QPushButton(Com::OK());
    ok->setMaximumWidth(80);

    methodtext = new QLabel(Com::ResamplingMethod());
    freqtext = new QLabel(Com::NewFreq());
    ekgtext = new QLabel(Com::EKG());

    methodBox = new QComboBox;
    freqedit = new QLineEdit("2");
    ekgBox = new QComboBox;

    methodBox->addItem(Com::PanTompkins());
    methodBox->addItem(Com::NadarayaWatson());
    methodBox->addItem(Com::WithoutResampl());
    methodBox->setCurrentIndex(0);

    for (auto & it : names) {
        ekgBox->addItem(it);
    }

    this->setLayout(mainbox);
    mainbox->addLayout(gridbox);
    mainbox->addLayout(okbox);
    okbox->addWidget(ok);
    gridbox->addWidget(methodtext,0,0);
    gridbox->addWidget(freqtext,1,0);
    gridbox->addWidget(methodBox,0,1);
    gridbox->addWidget(freqedit,1,1);
    gridbox->addWidget(ekgtext,2,0);
    gridbox->addWidget(ekgBox,2,1);

    connect(ok, &QAbstractButton::clicked,this,&QWidget::close);
    void(QComboBox::*pfn)(int) = &QComboBox::currentIndexChanged;
    void(DialogResampleMethod::*pfnMethod)(int) = &DialogResampleMethod::onChangeMethod;
    void(DialogResampleMethod::*pfnEkg)(int) = &DialogResampleMethod::onChangeEkg;
    connect(methodBox,pfn,this,pfnMethod);
    connect(ekgBox,pfn,this,pfnEkg);

    void(QLineEdit::*pfn2)(const QString&) = &QLineEdit::textChanged;
    void(DialogResampleMethod::*pfnFreq)(const QString&) = &DialogResampleMethod::freqChange;
    connect(freqedit,pfn2,this,pfnFreq);

    void(DialogResampleMethod::*pfn3)(int) = &DialogResampleMethod::onSetIdentical;
    connect(methodBox,pfn,this,pfn3);
}

void DialogResampleMethod::onChangeMethod(int ind) {
    (void)ind;
    *methodInd = methodBox->currentIndex();
}

void DialogResampleMethod::onChangeEkg(int ind) {
    (void)ind;
    *ekgInd = ekgBox->currentIndex();
}

void DialogResampleMethod::onSetIdentical(int ind) {
    (void)ind;
    if (methodBox->currentText() == Com::WithoutResampl()) {
        ekgBox->hide();
        ekgtext->hide();
        freqtext->hide();
        freqedit->hide();
    }
    else {
        ekgBox->show();
        ekgtext->show();
        freqtext->show();
        freqedit->show();
    }
}

void DialogResampleMethod::freqChange(const QString & str) {
    (void)str;
    *freq=freqedit->text().toDouble();
}

DialogResampleData::DialogResampleData(std::shared_ptr<int> _setInd, std::shared_ptr<DataCollection> _datacollection, std::shared_ptr<QVector<bool> > _isVisible) :
    setInd(_setInd),
    isVisible(_isVisible),
    datacollection(_datacollection)
{
    this->setWindowTitle(Com::DataResample());
    this->setSizePolicy(QSizePolicy::Minimum,QSizePolicy::Minimum);


    mainbox = new QVBoxLayout;
    boxesbox = new QVBoxLayout;
    okbox = new QHBoxLayout;
    ok = new QPushButton(Com::OK());
    ok->setMaximumWidth(80);

    setdatatext = new QLabel(Com::ChooseDataSet());
    datatext = new QLabel(Com::CheckSignals());

    setBox = new QComboBox;

    for  (int i=0;i<datacollection->size();i++) {
        if ((*datacollection)[i]->type() == DataRaw::TypeString()) {
            setBox->addItem((*datacollection)[i]->tabname());
        }
    }

    this->setLayout(mainbox);
    mainbox->setAlignment(Qt::AlignTop);
    mainbox->addWidget(setdatatext);
    mainbox->addWidget(setBox);
    mainbox->addWidget(datatext);
    mainbox->addLayout(boxesbox);
    mainbox->addLayout(okbox);
    okbox->addWidget(ok);

    setdatatext->setAlignment(Qt::AlignHCenter);
    datatext->setAlignment(Qt::AlignHCenter);

    connect(ok, &QAbstractButton::clicked,this,&QWidget::close);
    void(QComboBox::*pfn1)(int) = &QComboBox::currentIndexChanged;
    void(DialogResampleData::*pfn2)(int) = &DialogResampleData::onChangeSet;
    connect(setBox,pfn1,this,pfn2);

    setBox->setCurrentIndex(0);
    onChangeSet(0);
}

void DialogResampleData::onChangeSet(int ind) {
    if (ind < 0)
        return;
    *setInd = ind;
    clearLayout(boxesbox);
    boxes.clear();
    isVisible->clear();
    QVector<QString> tmp = (*datacollection)[ind]->getNames();
    for (int i=0;i<tmp.size();i++) {
        boxes.push_back(new QCheckBox(tmp[i]));
        boxes[i]->setChecked(true);
        isVisible->push_back(true);
        boxesbox->addWidget(boxes[i]);
    }
    boxes.push_back(new QCheckBox(Com::EnableDisableAll()));
    boxes.last()->setChecked(false);
    boxesbox->addWidget(boxes.last());
    for (int i=0;i<boxes.size();i++) {
        connect(boxes[i],&QCheckBox::clicked,this,&DialogResampleData::onCheck);
    }
    this->repaint();
}

void DialogResampleData::onCheck() {
    bool all = true;
    for (int i=0;i<isVisible->size();i++) {
        if ((*isVisible)[i] != boxes[i]->isChecked()) {
            all = false;
            break;
        }
    }
    if (all) {
        for (int i=0;i<isVisible->size();i++) {
            boxes[i]->setChecked(boxes.last()->isChecked());
            (*isVisible)[i] = boxes.last()->isChecked();
        }
    }
    else {
        for (int i=0;i<isVisible->size();i++) {
            (*isVisible)[i] = boxes[i]->isChecked();
        }
    }
}

void clearLayout(QLayout *layout) {
    QLayoutItem *item;
    while((item = layout->takeAt(0))) {
        if (item->layout()) {
            clearLayout(item->layout());
            delete item->layout();
        }
        if (item->widget()) {
            delete item->widget();
        }
        delete item;
    }
}
