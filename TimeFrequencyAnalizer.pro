#-------------------------------------------------
#
# Project created by QtCreator 2016-08-02T14:21:58
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets printsupport

TARGET = TimeFrequencyAnalizer
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    module.cpp \
    modulefile.cpp \
    testmodule.cpp \
    data.cpp \
    dataraw.cpp \
    qcustomplot.cpp \
    dialograwdata.cpp \
    moduleresample.cpp \
    dataresample.cpp \
    dialogresampledata.cpp \
    com.cpp \
    plotfunctions.cpp \
    moduletfr.cpp \
    datatfr.cpp \
    dialogtfrdata.cpp \
    hilbert.cpp \
    fft.cpp \
    tfrzam.cpp \
    resampling.cpp

HEADERS  += mainwindow.h \
    module.h \
    usings.h \
    testmodule.h \
    modulefile.h \
    data.h \
    dataraw.h \
    qcustomplot.h \
    dialograwdata.h \
    moduleresample.h \
    dataresample.h \
    dialogresampledata.h \
    com.h \
    plotfunctions.h \
    moduletfr.h \
    datatfr.h \
    dialogtfrdata.h \
    hilbert.h \
    fft.h \
    tfrzam.h \
    resampling.h

FORMS    += mainwindow.ui

DISTFILES +=

RESOURCES += \
    images.qrc
