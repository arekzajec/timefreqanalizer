#ifndef RESAMPLING_H
#define RESAMPLING_H

#include <dataraw.h>
RealMatrix resampleUsingEkg(RealMatrix sig, QVector<double> ekg, double orgfreq, double newfreq);
RealMatrix detrend(RealMatrix sig);
RealMatrix pan_tompkin(QVector<double> ekg, double fs); //qrs_amp, qrs_i, delay

double Epanechnikov(double x);
QVector<double> NadarayaWatson(QVector<double> x, QVector<double> y, double newfs, double (*ker)(double in), double h);
RealMatrix NWresampling(RealMatrix in, double oldfq, double newfq, double (*ker)(double in), double h);

#endif // RESAMPLING_H
