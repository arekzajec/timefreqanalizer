#include "dataraw.h"

int DataRaw::counter = 0;

DataRaw::DataRaw(QTabWidget *tb) :
    Data(tb),
    timeInd(new int(-1)),
    freq(new double(200.0))
{
    counter++;
}

DataRaw::~DataRaw() {
    counter--;
}

void DataRaw::readCSVFileData(QFile *file, QString tabname, std::unique_ptr<QMessageBox> _msgbox) {
    _tabname = tabname;
    QStringList wordList;
    QByteArray line;
    line = file->readLine();
    for (auto & it : line.split(',')) {
        wordList.append(it.trimmed());
    }
    names.reserve(wordList.size());
    data.reserve(wordList.size());
    isVisible = std::shared_ptr<QVector<bool>>(new QVector<bool>);
    isVisible->reserve(wordList.size());
    for (int i=0;i<wordList.size();i++) {
        data.push_back(QVector<double>());
        isVisible->push_back(true);
    }
    bool ok = false;
    wordList.first().toDouble(&ok);
    if (ok) {
        for (int i=1;i<=wordList.size();i++) {
            QString var = Com::Variable();
            var.append(QString::number(i));
            names.push_back(var);
        }
    }
    else {
        for (auto & it : wordList) {
            names.push_back(it);
        }
        line.clear();
    }
    double tmpdbl;
    while(!file->atEnd()) {
        if (line.isEmpty())
            line = file->readLine();
        int i=0;
        for (auto & it : line.split(',')) {
            ok = false;
            tmpdbl = it.trimmed().toDouble(&ok);
            if (ok) {
                data[i].push_back(tmpdbl);
            }
            else {
                data[i].push_back(std::numeric_limits<double>::quiet_NaN());
            }
            i++;
        }
        line.clear();
        i=0;
    }
    _msgbox->done(0);
    DialogRawDataTime *dialograwdatatime = new DialogRawDataTime(timeInd,freq,names);
    dialograwdatatime->exec();
    if (timeInd >= 0)
        (*isVisible)[*timeInd] = false;
    recalculateTime();
    DialogRawDataChose *dialograwdatachoose = new DialogRawDataChose(timeInd,names,isVisible);
    dialograwdatachoose->exec();
}

void DataRaw::recalculateTime() {
    if (*timeInd == -1) {
        time.resize(data[0].size());
        for (int i=0;i<time.size();i++) {
            time[i] = i/(*freq);
        }
        return;
    }
    if (*timeInd < 0)
        return;
    time = data[*timeInd];
    double first = time[0];
    for (auto & it : time) {
        it -= first;
        it *= 86400.0;
    }
    return;
}

void DataRaw::display() {
    QWidget *rawTab = new QWidget();
    tabBar->addTab(rawTab,_tabname);
    tabBar->setCurrentWidget(rawTab);

    setDefaultColor(rawTab);
    QVBoxLayout *vlayout = new QVBoxLayout();

    QScrollArea *scrollArea = new QScrollArea();

    scrollArea->setVerticalScrollBarPolicy(Qt::ScrollBarAsNeeded);
    scrollArea->setHorizontalScrollBarPolicy(Qt::ScrollBarAsNeeded);
    scrollArea->setWidgetResizable(true);

    QVBoxLayout *scrollvlayout = new QVBoxLayout();
    for (int i=0;i<data.size();i++) {
        if ((*isVisible)[i])
            addPlot(scrollvlayout,i);
    }
    QWidget *scrollWidget = new QWidget();
    scrollWidget->setLayout(scrollvlayout);
    scrollWidget->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);

    scrollArea->setWidget(scrollWidget);
    vlayout->addWidget(scrollArea);
    rawTab->setLayout(vlayout);
    vlayout->setMargin(0);
    scrollvlayout->setMargin(0);
}

void DataRaw::addPlot(QVBoxLayout *layout, int ind) {
    addPlotGlob(layout,data[ind],time,names[ind]);
}

void* DataRaw::getData() {
    //std::shared_ptr<RealMatrix> tmp(new RealMatrix);
    tmpOutData = std::shared_ptr<RealMatrix>(new RealMatrix);
    for (int i=0;i<data.size();i++) {
        if ((*isVisible)[i])
            tmpOutData->push_back(data[i]);
    }
    //return tmp.get();
    return tmpOutData.get();
}

QVector<QString> DataRaw::getNames() {
    QVector<QString> tmp;
    for (int i=0;i<names.size();i++) {
        if ((*isVisible)[i])
            tmp.push_back(names[i]);
    }
    return tmp;
}

QVector<double> DataRaw::getTime() {
    return time;
}

double DataRaw::getFreq() {
    return *freq;
}
