#ifndef COM_H
#define COM_H

#include <QString>

class Com{
public:
    Com();
    static QString WindowTitle() {return QString("TimeFrequencyAnalizer");}
    static QString Start() {return QString("Start");}
    static QString Variable() {return QString("Variable ");}
    static QString TimeSec() {return QString("time [sec]");}
    static QString ResampledData() {return QString("Resampled Data");}
    static QString TimeSamples() {return QString("Time samples");}
    static QString OK() {return QString("OK");}
    static QString ChooseTimeSamples() {return QString("Choose time samples");}
    static QString SampleingFreq() {return QString("Samling frequency [Hz]");}
    static QString NoneDataTime() {return QString("None - set sampling frequency");}
    static QString CheckDisp() {return QString("Check signals to display");}
    static QString EnableDisableAll() {return QString("Enable/disable All");}
    static QString ResamplingMethod() {return QString("Resampling method");}
    static QString NewFreq() {return QString("New frequency [Hz]");}
    static QString EKG() {return QString("EKG signal");}
    static QString PanTompkins() {return QString("Pan-Tompkins");}
    static QString NadarayaWatson() {return QString("Nadaraya-Watson");}
    static QString DataResample() {return QString("Data to resample");}
    static QString ChooseDataSet() {return QString("Choose dataset");}
    static QString CheckSignals() {return QString("Check signals");}
    static QString OpenDataFile() {return QString("Open data file");}
    static QString OpenDataFileTip() {return QString("Open filewith signals data");}
    static QString CloseDataFile() {return QString("Close data file");}
    static QString CloseDataFileTip() {return QString("Close file with signals data");}
    static QString File() {return QString("File");}
    static QString SelectFileToOpen() {return QString("Select CSV file to open");}
    static QString SelectFileToOpenType() {return QString("CSV Files (*.csv)");}
    static QString DataLoadingTitle() {return QString("Data loading");}
    static QString DataLoadingMsg() {return QString("Loading data from the file. It may take few minutes.");}
    static QString RawData() {return QString("Raw data");}
    static QString Time() {return QString("Time");}
    static QString ResampleRawData() {return QString("Resample Raw Data");}
    static QString ChooseDataToResampling() {return QString("Choose Data to resampling");}
    static QString Resampling() {return QString("Resampling");}
    static QString Warning() {return QString("Warning");}
    static QString PleaseOpenDataFirst() {return QString("Please open some raw data first!");}
    static QString AboutProgram() {return QString("About program");}
    static QString InfoAboutProgram() {return QString("Information about program");}
    static QString Info() {return QString("Info");}
    static QString InfoAboutProgramAll() {return QString("Program version 0.1\nProgram created by BrainLab,\nWroclaw University of Science and Technology");}
    static QString WithoutResampl() {return QString("Without Resampling");}
    static QString CalculateTFR() {return QString("Calculate TFR representation");}
    static QString ChooseDataToTFR() {return QString("Choose signal to get TFR representation");}
    static QString TFR() {return QString("Time-Frequency Representation");}
    static QString TFRdata() {return QString("TFR data");}
    static QString DataTFR() {return QString("Data to TFR representation");}
    static QString TFRparam() {return QString("TFR parameters");}
    static QString TFRChooseAlgorithm() {return QString("TFR algorithm");}
    static QString TFRFreqBins() {return QString("Frequency Bins");}
    static QString TFRTimeSW() {return QString("Time Smoothing Window");}
    static QString TFRFreqSW() {return QString("Frequency Smoothing Window");}
    static QString LowFreq() {return QString("Low frequency");}
    static QString HiFreq() {return QString("High frequency");}
    static QString Type() {return QString("Type");}
    static QString Length() {return QString("Length");}
    static QString ZAM() {return QString("Zhao-Atlas-Marks");}
    static QString Hanning() {return QString("Hanning");}
    static QString FreqHz() {return QString("frequency [Hz]");}
    static QString Data2Choose() {return QString("Choose data");}
};

#endif // COM_H
