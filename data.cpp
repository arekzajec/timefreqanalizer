#include "data.h"

Data::Data(QTabWidget *tb) :
    tabBar(tb)
{

}

void setDefaultColor(QWidget *ptr) {
    QPalette pal(ptr->palette());
    pal.setColor(QPalette::Background, QColor(245,245,245));
    ptr->setAutoFillBackground(true);
    ptr->setPalette(pal);
    ptr->show();
}
