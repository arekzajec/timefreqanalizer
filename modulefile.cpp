#include "modulefile.h"
#include <QDebug>

ModuleFile::ModuleFile(QMainWindow *_ptr, DataCollection *_data, QTabWidget *tb, QHBoxLayout *buttonlay) :
    Module(_ptr, _data, tb, buttonlay)
{

}

void ModuleFile::addActions(Actions & actions) {
    actions["FileModule::openData"] = std::shared_ptr<QAction>(new QAction(Com::OpenDataFile(),ptr));
    actions["FileModule::openData"]->setShortcuts(QKeySequence::Open);
    actions["FileModule::openData"]->setIcon(QIcon(":/img/icons/open.ico"));
    actions["FileModule::openData"]->setStatusTip(Com::OpenDataFileTip());
    ptr->connect(actions["FileModule::openData"].get(),&QAction::triggered,this,&ModuleFile::openDataFile);

    actions["FileModule::closeData"] = std::shared_ptr<QAction>(new QAction(Com::CloseDataFile(),ptr));
    actions["FileModule::closeData"]->setShortcuts(QKeySequence::Close);
    actions["FileModule::closeData"]->setStatusTip(Com::CloseDataFileTip());
    ptr->connect(actions["FileModule::closeData"].get(),&QAction::triggered,this,&ModuleFile::closeDataFile);
}

void ModuleFile::addMenus(Menus & menus, Actions &actions) {
    menus["FileModule"] = std::shared_ptr<QMenu>(ptr->menuBar()->addMenu(Com::File()));
    menus["FileModule"].get()->addAction(actions["FileModule::openData"].get());
    menus["FileModule"].get()->addAction(actions["FileModule::closeData"].get());
}

void ModuleFile::addIcons(Icons &icons) {
    QPushButton *openPB = newIcon(QIcon(":/img/icons/open.ico"));
    icons["FileModule::openData"] = std::shared_ptr<QPushButton>(openPB);
    buttonslayout->addWidget(openPB);
    ptr->connect(openPB,&QAbstractButton::clicked,this,&ModuleFile::openDataFile);
}

void ModuleFile::openDataFile() {
    closeDataFile();
    filename = QFileDialog::getOpenFileName(nullptr,Com::SelectFileToOpen(),QStandardPaths::writableLocation(QStandardPaths::DocumentsLocation),Com::SelectFileToOpenType());
    file = std::shared_ptr<QFile>(new QFile(filename));
    if (!file->open(QIODevice::ReadOnly)) {
        qDebug() << file->errorString();
        file->close();
        filename.clear();

        return;
    }
    std::unique_ptr<QMessageBox> WaitMsg(new QMessageBox(QMessageBox::Information, Com::DataLoadingTitle(), Com::DataLoadingMsg(), QMessageBox::NoButton));
    WaitMsg->setStandardButtons(0);
    WaitMsg->show();
    DataRaw *dane = new DataRaw(tabBar);
    data->push_back(dane);
    dane->readCSVFileData(file.get(),QString(Com::RawData()).append(QString::number(dane->getCount())),std::move(WaitMsg));
    dane->display();
}

void ModuleFile::closeDataFile() {

}
