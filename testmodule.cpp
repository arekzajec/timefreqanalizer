#include "testmodule.h"
#include "QDebug"

TestModule::TestModule(QMainWindow *_ptr, DataCollection *_data, QTabWidget *tb, QHBoxLayout *buttonlay) :
    Module(_ptr,_data,tb,buttonlay)
{

}

void TestModule::addActions(Actions & actions) {
    actions["TestModule::test"] = std::shared_ptr<QAction>(new QAction(Com::AboutProgram(),ptr));
    actions["TestModule::test"]->setStatusTip(Com::InfoAboutProgram());
    ptr->connect(actions["TestModule::test"].get(),&QAction::triggered,this,&TestModule::test_fcn);
}

void TestModule::addMenus(Menus & menus, Actions &actions) {
    menus["TestModule::test"] = std::shared_ptr<QMenu>(ptr->menuBar()->addMenu(Com::Info()));
    menus["TestModule::test"].get()->addAction(actions["TestModule::test"].get());
}

void TestModule::addIcons(Icons &icons) {

}

void TestModule::test_fcn() {
    QMessageBox(QMessageBox::Information, Com::AboutProgram(), Com::InfoAboutProgramAll(), QMessageBox::Ok).exec();

}
