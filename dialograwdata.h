#ifndef DIALOGRAWDATA_H
#define DIALOGRAWDATA_H

#include <QDialog>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QGridLayout>
#include <memory>
#include <QPushButton>
#include <QLabel>
#include <QLineEdit>
#include <QComboBox>
#include <QVector>
#include <QString>
#include <QDebug>
#include <QSpacerItem>
#include <QCheckBox>
#include "com.h"

class DialogRawDataTime : public QDialog
{
public:
    DialogRawDataTime(std::shared_ptr<int> _timeInd, std::shared_ptr<double> _freq, QVector<QString> _names);

private slots:
    void onChange(int ind);
    void freqChange(const QString & str);
private:
    std::shared_ptr<int> timeInd;
    std::shared_ptr<double> freq;
    QVector<QString> names;

    QVBoxLayout *mainbox;
    QGridLayout *gridbox;
    QHBoxLayout *okbox;
    QPushButton *ok;
    QLabel *timedatatext;
    QLabel *freqtext;
    QComboBox *timeIndBox;
    QLineEdit *freqedit;
};

class DialogRawDataChose : public QDialog
{
public:
    DialogRawDataChose(std::shared_ptr<int> _timeInd, QVector<QString> _names, std::shared_ptr<QVector<bool> > _isVisible);
private slots:
    void onClicked();

private:
    std::shared_ptr<int> timeInd;
    QVector<QString> names;
    std::shared_ptr<QVector<bool> > isVisible;
    QVector<QCheckBox*> boxes;
    QPushButton *ok;
    QVBoxLayout *mainbox;
    QHBoxLayout *okbox;
    QLabel *todo;
};

#endif // DIALOGRAWDATA_H

