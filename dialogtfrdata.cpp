#include "dialogtfrdata.h"

DialogTFRParams::DialogTFRParams(std::shared_ptr<int> _algorithmInd,
                                 std::shared_ptr<int> _numberOfFrequencyBins,
                                 std::shared_ptr<int> _timeSmoothingWindowInd,
                                 std::shared_ptr<int> _freqSmoothingWindowInd,
                                 std::shared_ptr<int> _timeSmoothingWindowLength,
                                 std::shared_ptr<int> _freqSmoothingWindowLength,
                                 std::shared_ptr<double> _lowfreq,
                                 std::shared_ptr<double> _hifreq) :
    algorithmInd(_algorithmInd),
    numberOfFrequencyBins(_numberOfFrequencyBins),
    timeSmoothingWindowInd(_timeSmoothingWindowInd),
    freqSmoothingWindowInd(_freqSmoothingWindowInd),
    timeSmoothingWindowLength(_timeSmoothingWindowLength),
    freqSmoothingWindowLength(_freqSmoothingWindowLength),
    lowfreq(_lowfreq),
    hifreq(_hifreq)
{
    this->setWindowTitle(Com::TFRparam());

    mainbox = new QVBoxLayout;
    okbox = new QHBoxLayout;
    gridbox = new QGridLayout;
/*    hbox1 = new QHBoxLayout;
    hbox2 = new QHBoxLayout;
    gridbox1 = new QGridLayout;
    gridbox2 = new QGridLayout;
    gridbox3 = new QGridLayout;
    gridbox4 = new QGridLayout;*/
    ok = new QPushButton(Com::OK());
    ok->setMaximumWidth(80);

    algorithmText = new QLabel(Com::TFRChooseAlgorithm());
    freqBinsText = new QLabel(Com::TFRFreqBins());
    timeSmoothingWindowText = new QLabel(Com::TFRTimeSW());
    freqSmoothingWindowText = new QLabel(Com::TFRFreqSW());
    lowfreqText = new QLabel(Com::LowFreq());
    hifreqText = new QLabel(Com::HiFreq());
    type1 = new QLabel(Com::Type());
    type2 = new QLabel(Com::Type());
    length1 = new QLabel(Com::Length());
    length2 = new QLabel(Com::Length());

    algorithmBox = new QComboBox;
    timeSmoothingWindowBox = new QComboBox;
    freqSmoothingWindowBox = new QComboBox;
    timeSmoothingWindowEdit = new QLineEdit("513");
    freqSmoothingWindowEdit = new QLineEdit("449");
    freqBinsEdit = new QLineEdit("4096");
    lowfreqEdit = new QLineEdit("0.02");
    hifreqEdit = new QLineEdit("0.07");

    algorithmBox->addItem(Com::ZAM());
    timeSmoothingWindowBox->addItem(Com::Hanning());
    freqSmoothingWindowBox->addItem(Com::Hanning());

    this->setLayout(mainbox);

    mainbox->addLayout(gridbox);
    mainbox->addLayout(okbox);
    okbox->addWidget(ok);
    gridbox->addWidget(algorithmText,0,0);
    gridbox->addWidget(algorithmBox,0,1);
    gridbox->addWidget(freqBinsText,1,0);
    gridbox->addWidget(freqBinsEdit,1,1);
    gridbox->addWidget(timeSmoothingWindowText,2,0);
    gridbox->addWidget(type1,3,0);
    gridbox->addWidget(length1,4,0);
    gridbox->addWidget(timeSmoothingWindowBox,3,1);
    gridbox->addWidget(timeSmoothingWindowEdit,4,1);
    gridbox->addWidget(freqSmoothingWindowText,5,0);
    gridbox->addWidget(type2,6,0);
    gridbox->addWidget(length2,7,0);
    gridbox->addWidget(freqSmoothingWindowBox,6,1);
    gridbox->addWidget(freqSmoothingWindowEdit,7,1);
    gridbox->addWidget(lowfreqText,8,0);
    gridbox->addWidget(hifreqText,9,0);
    gridbox->addWidget(lowfreqEdit,8,1);
    gridbox->addWidget(hifreqEdit,9,1);

    /*mainbox->addLayout(gridbox1);
    mainbox->addLayout(hbox1);
    mainbox->addLayout(gridbox2);
    mainbox->addLayout(hbox2);
    mainbox->addLayout(gridbox3);
    mainbox->addLayout(gridbox4);
    mainbox->addLayout(okbox);
    okbox->addWidget(ok);
    gridbox1->addWidget(algorithmText,0,0);
    gridbox1->addWidget(algorithmBox,0,1);
    gridbox1->addWidget(freqBinsText,1,0);
    gridbox1->addWidget(freqBinsEdit,1,1);
    hbox1->addWidget(timeSmoothingWindowText);
    gridbox2->addWidget(type1,0,0);
    gridbox2->addWidget(length1,0,1);
    gridbox2->addWidget(timeSmoothingWindowBox,1,0);
    gridbox2->addWidget(timeSmoothingWindowEdit,1,1);
    hbox2->addWidget(freqSmoothingWindowText);
    gridbox3->addWidget(type2,0,0);
    gridbox3->addWidget(length2,0,1);
    gridbox3->addWidget(freqSmoothingWindowBox,1,0);
    gridbox3->addWidget(freqSmoothingWindowEdit,1,1);
    gridbox4->addWidget(lowfreqText,0,0);
    gridbox4->addWidget(hifreqText,0,1);
    gridbox4->addWidget(lowfreqEdit,1,0);
    gridbox4->addWidget(hifreqEdit,1,1);*/

    connect(ok,&QAbstractButton::clicked,this,&QWidget::close);
    void(QComboBox::*pfn)(int) = &QComboBox::currentIndexChanged;
    void(DialogTFRParams::*pfnAlg)(int) = &DialogTFRParams::onChangeAlg;
    void(DialogTFRParams::*pfnTSW)(int) = &DialogTFRParams::onChangeAlgTimeW;
    void(DialogTFRParams::*pfnFSW)(int) = &DialogTFRParams::onChangeAlgFreqW;
    connect(algorithmBox,pfn,this,pfnAlg);
    connect(timeSmoothingWindowBox,pfn,this,pfnTSW);
    connect(freqSmoothingWindowBox,pfn,this,pfnFSW);

    void(QLineEdit::*pfn2)(const QString&) = &QLineEdit::textChanged;
    void(DialogTFRParams::*pfnFreqBins)(const QString&) = &DialogTFRParams::onChangeFreqBins;
    void(DialogTFRParams::*pfnTimeWLength)(const QString&) = &DialogTFRParams::onChangeTimeWLength;
    void(DialogTFRParams::*pfnFreqWLength)(const QString&) = &DialogTFRParams::onChangeFreqWLength;
    void(DialogTFRParams::*pfnLowFreq)(const QString&) = &DialogTFRParams::onChangeLowFreq;
    void(DialogTFRParams::*pfnHiFreq)(const QString&) = &DialogTFRParams::onChangeHiFreq;

    connect(freqBinsEdit,pfn2,this,pfnFreqBins);
    connect(timeSmoothingWindowEdit,pfn2,this,pfnTimeWLength);
    connect(freqSmoothingWindowEdit,pfn2,this,pfnFreqWLength);
    connect(lowfreqEdit,pfn2,this,pfnLowFreq);
    connect(hifreqEdit,pfn2,this,pfnHiFreq);

}

void DialogTFRParams::onChangeAlg(int ind) {
    (void)ind;
    *algorithmInd = algorithmBox->currentIndex();
}

void DialogTFRParams::onChangeAlgTimeW(int ind) {
    (void)ind;
    *timeSmoothingWindowInd = timeSmoothingWindowBox->currentIndex();
}

void DialogTFRParams::onChangeAlgFreqW(int ind) {
    (void)ind;
    *freqSmoothingWindowInd = freqSmoothingWindowBox->currentIndex();
}

void DialogTFRParams::onChangeFreqBins(const QString &str) {
    (void)str;
    *numberOfFrequencyBins=freqBinsEdit->text().toDouble();
}

void DialogTFRParams::onChangeTimeWLength(const QString &str) {
    (void)str;
    *timeSmoothingWindowLength=timeSmoothingWindowEdit->text().toDouble();
}

void DialogTFRParams::onChangeFreqWLength(const QString &str) {
    (void)str;
    *freqSmoothingWindowLength=freqSmoothingWindowEdit->text().toDouble();
}

void DialogTFRParams::onChangeLowFreq(const QString &str) {
    (void)str;
    *lowfreq=lowfreqEdit->text().toDouble();
}

void DialogTFRParams::onChangeHiFreq(const QString &str) {
    (void)str;
    *hifreq=hifreqEdit->text().toDouble();
}



DialogTFRData::DialogTFRData(std::shared_ptr<int> _setInd, std::shared_ptr<DataCollection> _datacollection, std::shared_ptr<QVector<bool> > _isVisible) :
    setInd(_setInd),
    isVisible(_isVisible),
    datacollection(_datacollection)
{
    this->setWindowTitle(Com::DataTFR());
    this->setSizePolicy(QSizePolicy::Minimum,QSizePolicy::Minimum);

    mainbox = new QVBoxLayout;
    boxesbox = new QVBoxLayout;
    okbox = new QHBoxLayout;
    ok = new QPushButton(Com::OK());
    ok->setMaximumWidth(80);

    setdatatext = new QLabel(Com::ChooseDataSet());
    datatext = new QLabel(Com::CheckSignals());

    setBox = new QComboBox;

    for  (int i=0;i<datacollection->size();i++) {
        if ((*datacollection)[i]->type() == DataRaw::TypeString() || (*datacollection)[i]->type() == DataResample::TypeString()) {
            setBox->addItem((*datacollection)[i]->tabname());
        }
    }

    this->setLayout(mainbox);
    mainbox->setAlignment(Qt::AlignTop);
    mainbox->addWidget(setdatatext);
    mainbox->addWidget(setBox);
    mainbox->addWidget(datatext);
    mainbox->addLayout(boxesbox);
    mainbox->addLayout(okbox);
    okbox->addWidget(ok);

    setdatatext->setAlignment(Qt::AlignHCenter);
    datatext->setAlignment(Qt::AlignHCenter);

    connect(ok, &QAbstractButton::clicked,this,&QWidget::close);
    void(QComboBox::*pfn1)(int) = &QComboBox::currentIndexChanged;
    void(DialogTFRData::*pfn2)(int) = &DialogTFRData::onChangeSet;
    connect(setBox,pfn1,this,pfn2);

    setBox->setCurrentIndex(0);
    onChangeSet(0);
}

void DialogTFRData::onChangeSet(int ind) {
    if (ind < 0) {
        return;
    }
    *setInd=ind;
    clearLayout(boxesbox);
    boxes.clear();
    isVisible->clear();
    QVector<QString> tmp = (*datacollection)[ind]->getNames();
    for (int i=0;i<tmp.size();i++) {
        boxes.push_back(new QCheckBox(tmp[i]));
        boxes[i]->setChecked(true);
        isVisible->push_back(true);
        boxesbox->addWidget(boxes[i]);
    }
    boxes.push_back(new QCheckBox(Com::EnableDisableAll()));
    boxes.last()->setChecked(false);
    boxesbox->addWidget(boxes.last());
    for (int i=0;i<boxes.size();i++) {
        connect(boxes[i],&QCheckBox::clicked,this,&DialogTFRData::onCheck);
    }
    this->repaint();
}

void DialogTFRData::onCheck() {
    bool all = true;
    for (int i=0;i<isVisible->size();i++) {
        if ((*isVisible)[i] != boxes[i]->isChecked()) {
            all = false;
            break;
        }
    }
    if (all) {
        for (int i=0;i<isVisible->size();i++) {
            boxes[i]->setChecked(boxes.last()->isChecked());
            (*isVisible)[i] = boxes.last()->isChecked();
        }
    }
    else {
        for (int i=0;i<isVisible->size();i++) {
            (*isVisible)[i] = boxes[i]->isChecked();
        }
    }
}
