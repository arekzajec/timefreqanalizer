#include "moduletfr.h"

ModuleTFR::ModuleTFR(QMainWindow *_ptr, DataCollection *_data, QTabWidget *tb, QHBoxLayout *buttonlay) :
    Module(_ptr, _data, tb, buttonlay)
{

}

void ModuleTFR::addActions(Actions &actions) {
    actions["TFRModule::calculateTFR"] = std::shared_ptr<QAction>(new QAction(Com::CalculateTFR(),ptr));
    actions["TFRModule::calculateTFR"]->setStatusTip(Com::ChooseDataToTFR());
    actions["TFRModule::calculateTFR"]->setIcon(QIcon(":/img/icons/tfr.ico"));
    ptr->connect(actions["TFRModule::calculateTFR"].get(),&QAction::triggered,this,&ModuleTFR::calculateTFR);
}

void ModuleTFR::addMenus(Menus &menus, Actions &actions) {
    menus["TFRModule"] = std::shared_ptr<QMenu>(ptr->menuBar()->addMenu(Com::TFR()));
    menus["TFRModule"].get()->addAction(actions["TFRModule::calculateTFR"].get());
}

void ModuleTFR::addIcons(Icons &icons) {
    QPushButton *calcTFR_PB = newIcon(QIcon(":/img/icons/tfr.ico"));
    icons["TRFModule::calculateTFR"] = std::shared_ptr<QPushButton>(calcTFR_PB);
    buttonslayout->addWidget(calcTFR_PB);
    ptr->connect(calcTFR_PB,&QAbstractButton::clicked,this,&ModuleTFR::calculateTFR);
}

void ModuleTFR::calculateTFR() {
    bool stop = true;
    for (int i=0;i<data->size();i++) {
        if ((*data)[i]->type() == DataRaw::TypeString() || (*data)[i]->type() == DataResample::TypeString()) {
            stop = false;
            break;
        }
    }
    if (stop) {
        QMessageBox(QMessageBox::Warning, Com::Warning(),Com::PleaseOpenDataFirst(), QMessageBox::Ok).exec();
        return;
    }


    DataTFR *dane = new DataTFR(tabBar);
    data->push_back(dane);
    dane->calculate(data);
    dane->display();
}

