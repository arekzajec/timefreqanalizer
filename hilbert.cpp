#include "hilbert.h"
#include <QDebug>

/*v nie może być pusty!, dłgość v musi być potęgą 2*/
ComplexVector hilbert(QVector<double> & v) {
    qDebug() << "v " << v;
    int N = v.size();
    qDebug() << "N " << N;
    CArray tmpValArray(v.size());
    CArray h (N);
    h.resize(N);
    auto it=std::begin(tmpValArray);
    for (int i=0;i<v.size();i++) {
        *it++ = v[i];
    }
    fft(tmpValArray);
    qDebug() << "after fft";
    for (auto & it : tmpValArray) {
        qDebug() << it.real() << it.imag();
    }
    if (N%2) { //odd
        h[0]=1;
        for (int i=1;i<=N/2;i++) {
            h[i]=2;
        }
    }
    else { //even
        h[0]=1;
        h[N/2]=1;
        for (int i=1;i<N/2;i++) {
            h[i]=2;
        }
    }
    CArray out = tmpValArray * h;
    ifft(out);
    qDebug() << "output";
    for (auto & it : out) {
        qDebug() << it.real() << it.imag();
    }
    ComplexVector vout;
    vout.reserve(out.size());
    for (int i=0;i<out.size();i++) {
        vout.push_back(std::complex<double>(out[i].real(),out[i].imag()));
    }
    return vout;
}
