#include "dataresample.h"
#include <QDebug>

int DataResample::counter = 0;

DataResample::DataResample(QTabWidget *tb) :
    Data(tb),
    freq(new double(2)),
    methodInd(new int(-1)),
    ekgInd(new int(-1)),
    rawInd(new int(-1))
{
    counter++;
}

DataResample::~DataResample() {
    counter--;
}

void DataResample::calculate(std::shared_ptr<DataCollection> data_collection) {
    _tabname = QString(Com::ResampledData()).append(QString::number(getCount()));
    std::shared_ptr<int> setInd(new int);
    isVisible = std::shared_ptr<QVector<bool>>(new QVector<bool>);

    DialogResampleData *dialogData = new DialogResampleData(setInd,data_collection,isVisible);
    dialogData->exec();

    //QVector<QString> tmp = (*data_collection)[*setInd]->getNames();
    names = (*data_collection)[*setInd]->getNames();
    time = (*data_collection)[*setInd]->getTime();
    /*for (int i=0;i<tmp.size();i++) {
        if ((*isVisible)[i])
            names.push_back(tmp[i]);
    }*/

    DialogResampleMethod *dialogMethod = new DialogResampleMethod(methodInd,ekgInd,freq,names);
    dialogMethod->exec();

    switch(*methodInd) {
    case 0:{
        int size = static_cast<RealMatrix*>((*data_collection)[*setInd]->getData())->size();
        for (int i=0;i<size;i++) {
            if ((*isVisible)[i]) {
                data.push_back(static_cast<RealMatrix*>((*data_collection)[*setInd]->getData())->at(i));
            }
        }
        data = resampleUsingEkg(data,
                                static_cast<RealMatrix*>((*data_collection)[*setInd]->getData())->at(*ekgInd),
                                (*data_collection)[*setInd]->getFreq(),
                                *freq);
        data = detrend(data);
    };break;
    case 1:{
        int size = static_cast<RealMatrix*>((*data_collection)[*setInd]->getData())->size();
        for (int i=0;i<size;i++) {
            if ((*isVisible)[i]) {
                data.push_back(static_cast<RealMatrix*>((*data_collection)[*setInd]->getData())->at(i));
            }
        }
        data = NWresampling(data,(*data_collection)[*setInd]->getFreq(),*freq,Epanechnikov,0.01);//todo
        time.clear();
        for(int i=0;i<data[0].size();i++) {
            time.push_back(i / *freq);
        }
        qDebug() << data.size();
        qDebug() << data[0].size();
        qDebug() << data;
    };break;
    case 2:{
        *freq = (*data_collection)[*setInd]->getFreq();
        int size = static_cast<RealMatrix*>((*data_collection)[*setInd]->getData())->size();
        for (int i=0;i<size;i++) {
            if ((*isVisible)[i]) {
                data.push_back(static_cast<RealMatrix*>((*data_collection)[*setInd]->getData())->at(i));
            }
        }
    };break;
    default:break;
    }
}

void DataResample::display() {
    QWidget *resamplTab = new QWidget();
    tabBar->addTab(resamplTab,_tabname);
    tabBar->setCurrentWidget(resamplTab);

    setDefaultColor(resamplTab);
    QVBoxLayout *vlayout = new QVBoxLayout();

    QScrollArea *scrollArea = new QScrollArea();

    scrollArea->setVerticalScrollBarPolicy(Qt::ScrollBarAsNeeded);
    scrollArea->setHorizontalScrollBarPolicy(Qt::ScrollBarAsNeeded);
    scrollArea->setWidgetResizable(true);

    QVBoxLayout *scrollvlayout = new QVBoxLayout();
    for (int i=0;i<data.size();i++) {
        addPlot(scrollvlayout,i);
    }
    QWidget *scrollWidget = new QWidget();
    scrollWidget->setLayout(scrollvlayout);
    scrollWidget->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);

    scrollArea->setWidget(scrollWidget);
    vlayout->addWidget(scrollArea);
    resamplTab->setLayout(vlayout);
    vlayout->setMargin(0);
    scrollvlayout->setMargin(0);
}

void DataResample::addPlot(QVBoxLayout *layout, int ind) {
    addPlotGlob(layout,data[ind],time,names[ind]);
}

void *DataResample::getData() {
    return &data;
}

QVector<QString> DataResample::getNames() {
    return names;
}

QVector<double> DataResample::getTime() {
    return time;
}

double DataResample::getFreq() {
    return *freq;
}
