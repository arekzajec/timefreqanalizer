#include "dialograwdata.h"

DialogRawDataTime::DialogRawDataTime(std::shared_ptr<int> _timeInd, std::shared_ptr<double> _freq, QVector<QString> _names) :
    timeInd(_timeInd),
    freq(_freq),
    names(_names)
{
    this->setWindowTitle(Com::Time());

    mainbox = new QVBoxLayout;
    okbox = new QHBoxLayout;
    gridbox = new QGridLayout;
    ok = new QPushButton(Com::OK());
    ok->setMaximumWidth(80);
    timedatatext = new QLabel(Com::ChooseTimeSamples());
    freqtext = new QLabel(Com::SampleingFreq());
    timeIndBox = new QComboBox;
    freqedit = new QLineEdit("200");

    for (auto & it : names) {
        timeIndBox->addItem(it);
    }
    timeIndBox->addItem(Com::NoneDataTime());
    timeIndBox->setCurrentIndex(names.size());

    this->setLayout(mainbox);
    mainbox->addLayout(gridbox);
    mainbox->addLayout(okbox);
    okbox->addWidget(ok);
    gridbox->addWidget(timedatatext,0,0);
    gridbox->addWidget(freqtext,1,0);
    gridbox->addWidget(timeIndBox,0,1);
    gridbox->addWidget(freqedit,1,1);

    connect(ok, &QAbstractButton::clicked,this,&QWidget::close);
    void(QComboBox::*pfn1)(int) = &QComboBox::currentIndexChanged;
    void(DialogRawDataTime::*pfn2)(int) = &DialogRawDataTime::onChange;
    connect(timeIndBox,pfn1,this,pfn2);
    void(QLineEdit::*pfn3)(const QString&) = &QLineEdit::textChanged;
    void(DialogRawDataTime::*pfn4)(const QString&) = &DialogRawDataTime::freqChange;
    connect(freqedit,pfn3,this,pfn4);
}

void DialogRawDataTime::onChange(int ind) {
    (void)ind;
    if (names.size() == timeIndBox->currentIndex()) {
        *timeInd = -1;
        freqedit->show();
        freqtext->show();
    }
    else {
        *timeInd = timeIndBox->currentIndex();
        freqedit->hide();
        freqtext->hide();

    }
}

void DialogRawDataTime::freqChange(const QString & str) {
    (void)str;
    *freq=freqedit->text().toDouble();
}

DialogRawDataChose::DialogRawDataChose(std::shared_ptr<int> _timeInd, QVector<QString> _names, std::shared_ptr<QVector<bool> > _isVisible) :
    names(_names),
    isVisible(_isVisible),
    timeInd(_timeInd)
{
    this->setWindowTitle(Com::Data2Choose());

    mainbox = new QVBoxLayout;
    okbox = new QHBoxLayout;
    ok = new QPushButton(Com::OK());
    ok->setMaximumWidth(80);
    todo = new QLabel(Com::CheckDisp());
    mainbox->addWidget(todo);
    boxes.reserve(isVisible->size());
    for (int i=0;i<isVisible->size();i++) {
        boxes.push_back(new QCheckBox(names[i]));
        if (i != *timeInd) {
            boxes[i]->setChecked(true);
            (*isVisible)[i] = true;
        }
        else {
            boxes[i]->setChecked(false);
            (*isVisible)[i] = false;
        }
        mainbox->addWidget(boxes[i]);
    }
    boxes.push_back(new QCheckBox(Com::EnableDisableAll()));
    boxes.last()->setChecked(false);
    mainbox->addWidget(boxes.last());
    mainbox->addLayout(okbox);
    okbox->addWidget(ok);
    this->setLayout(mainbox);

    connect(ok, &QAbstractButton::clicked,this,&QWidget::close);
    for (int i=0;i<boxes.size();i++) {
        connect(boxes[i],&QCheckBox::clicked,this,&DialogRawDataChose::onClicked);
    }
}

void DialogRawDataChose::onClicked() {
    bool all = true;
    for (int i=0;i<isVisible->size();i++) {
        if ((*isVisible)[i] != boxes[i]->isChecked()) {
            all = false;
            break;
        }
    }
    if (all) {
        for (int i=0;i<isVisible->size();i++) {
            boxes[i]->setChecked(boxes.last()->isChecked());
            (*isVisible)[i] = boxes.last()->isChecked();
        }
    }
    else {
        for (int i=0;i<isVisible->size();i++) {
            (*isVisible)[i] = boxes[i]->isChecked();
        }
    }
}
