#ifndef TESTMODULE_H
#define TESTMODULE_H

#include "module.h"
#include "com.h"

/*
 * testmodule - about
 */

class TestModule : public Module
{
public:
    TestModule(QMainWindow *_ptr, DataCollection *_data, QTabWidget *tb, QHBoxLayout *buttonlay);
    void addActions(Actions & actions);
    void addMenus(Menus & menus, Actions & actions);
    void addIcons(Icons & icons);
    void test_fcn();
};


#endif // TESTMODULE_H

