#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <usings.h>
#include <QMainWindow>
#include "module.h"
#include "testmodule.h"
#include "modulefile.h"
#include "moduleresample.h"
#include <moduletfr.h>
#include "mainwindowinterface.h"
#include "com.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();    

private slots:
    void closeTab(int ind);

private:
    Menus menus_ptrs;
    Actions actions_ptrs;
    Icons icons_ptrs;

    Ui::MainWindow *ui;

    QVector<Module*> modules;
    DataCollection data;

    QTabWidget *tabBar;
    QVBoxLayout *mainlayout;
    QWidget *central;
    //QHBoxLayout *upericonslayout;
    //QWidget *iconswidget;
    QHBoxLayout *iconslayout;

    void createMenus();
    void createActions();
    void createIcons();
    void createFunctionsSlots();
    void connectActionsAndFunctions();

};

#endif // MAINWINDOW_H
