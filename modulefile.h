#ifndef MODULEFILE_H
#define MODULEFILE_H
#include "module.h"
#include <QFileDialog>
#include <QStandardPaths>
#include <QFile>
#include <QString>
#include "dataraw.h"

class ModuleFile : public Module
{
    QString filename;
    std::shared_ptr<QFile> file;
public:
    QPushButton* openicon;

    ModuleFile(QMainWindow *_ptr, DataCollection *_data, QTabWidget *tb, QHBoxLayout *buttonlay);
    void addActions(Actions & actions);
    void addMenus(Menus & menus, Actions & actions);
    void addIcons(Icons & icons);
    void openDataFile();
    void closeDataFile();
};

#endif // MODULEFILE_H
